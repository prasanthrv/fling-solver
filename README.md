README
===================


This document is intented to serve as both an explanation for the **design process** of the software as well as how it can be **used**.

----------


Design
-------------


> **Assumptions:**

> - The grid is 8x7 always and does not handle huge board sizes.
> - Familiarity with pure functional languages and lazy evaluation.

#### Data.Map

**Data.Map** was chosen as the data structure for storing the indices of every specific row and column. 
For a linear search of elements, it would be a search of 55 elements for both row and column, in worst case, ie. 110 elements for getting a list of moves. 
However Data.Map reduces this to 7+6 = 15, in the worst case.
Data.Map also just stores pointers to the list, which itself points to the data elements. So in essence the balls are reffered to thrice but stored only once in memory.

Another option was to construct a mxn two dimentional array, using `Data.Array`, however it is very clunky and reduces readability and does not mesh well with the laziness of Haskell. 

####  String log

For the log, a list of `String` was chosen, because appending is constant time, and the move list would not be long. 
Trying to implement it in `Data.Text` only lead to more computation without any memory efficiency gain. 

#### Recurse with all valid move list

The original idea was to only recurse with the move of the next candidate ball, this saves computing all the move list. However when it was implemented, there needed to be a way maintain a list of balls, which made for unweildly code as well as problems differentiating between the recursive calls down one level into the solution tree and the one at the end of moves.

####  Using Lists

When trying for other strict evaluation containers such as `Data.Sequence` etc, the tradeoffs between code readability and performance was not found to be balanced. 

#### Testing

For testing, a collection of solvable and unsolvable problems are fed into the program and the output is matched.
The move list can be emulated and then seen if it results in a win state, but ultimately it's the same process that is used to generate the move list. 
Manual testing of the move list is the best way to see if the solution is valid.

----------

Workflow
-------------------

The solution uses the `Stack` toolchain for managing the project and it's lifecycle. 

> **Assumptions:**

> - The input data fed is well formatted and structured according to the samples in the problem statement.


#### Setup Stack and GHC

1. Install [Stack](http://docs.haskellstack.org/en/stable/README/#how-to-instal).
2. If any problem, check out the [Haskell Platform](https://www.haskell.org/downloads)
2. Decompress tarball
3. If step 1 needed to install GHC, find ghc version with
 `ghc --version`
 1. Edit `stack.yaml` , find the line with `resolver: ghc-7.10.3`  
 2. Replace `7.10.3` with the ghc version already installed 
3. Run `stack setup` at the base of the directory

#### Stack Commands
Stack commands are supposed to be run at the base directory.

* Run `stack build` to compile program
* Run `stack exec fling-exe <input_file_path>` to solve problems
* Run `stack test` to run the test cases 

####  Directory structure

* `app/`   contains source files
* `test/` contains test cases
* `lib/`   contains lib files, currently unused
* `.stack-work` is for stack to use as workspace

#### Testing

For testing, please update the `rootTestDir` variable to the root directory of the project in `test/Spec.hs`.


