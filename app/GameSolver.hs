-- Solver for the game of Fling!

module GameSolver
        ( solveProblem
         ,safeHead
        )
where

import qualified Data.Map as M
import System.Directory (getDirectoryContents, doesFileExist)
import Data.List (intercalate, sortBy)
import Data.Maybe (fromJust, isJust)
import Data.Char (toLower)
import System.IO

import GameDefs
import GameParser



{- OUTSIDE IO FUNC -}
-- solve problem given a file path
solveProblem :: Maybe String -> IO String
-- no argument
solveProblem Nothing = return "Please append the path of a valid input file containing problem to the command line argument"
solveProblem (Just path) = do 
                            -- check if file exists
                            fileExists <- doesFileExist path
                            if not fileExists
                            -- file does not exist
                            then return "File does not exist, please recheck path"
                            else do
                                -- read and pull contents
                                contents <- readFile path 
                                -- generate game state
                                let indices =  readGrid contents
                                let (rowMap, colMap) = genMaps indices
                                let gs = (indices, rowMap, colMap)
                                -- return solution wrapped in IO
                                return $ solveFling gs
       
{-
 - Base algorithm:
 - 1. Generate intial game state
 - 2. Call game solver with initial state
 - 3. Recursively solve gamestate
 -   3.1 If there is just one ball, return move log and come to top of call stack
 -   3.2 If there is no moves and there is more than one ball, then no solution for current state,
 -       recurse back with recent move log removed
 -   3.3 If there is a move, execute move, update move log and recurse into modified gamestate 
 -}

{- MAIN SOLVER LOGIC -}

-- solve the game of fling
solveFling :: GameState -> String
-- already solved
solveFling ((x:[]),_,_)    = "Nothing to solve"
-- solve initial gamestate
solveFling gs = case solver (newMoves gs) gs [] of 
                (_,False)  -> "Game has no solution"
                (str,True) -> unlines $ reverse str

-- solve given gamestate
-- Return (str,bool) -> bool is if solution found and str is move list
-- first arg is all the moves possible in all balls in a specific gamestate
solver :: [(BallIndex, Direction)] -> GameState -> [String] -> ([String], Bool)
-- no moves left
solver []              gs@(balls,_,_) str = if length balls == 1 
                                            -- solution, only one ball left in board
                                            then (str,True)
                                            -- no moves, so come up one level, remove move log
                                            else (tail str, False)
-- moves present
solver ((ball,dir):ms) gs@(balls,_,_) str = if length balls == 1
                                            -- solution, only one ball left in board
                                            then (str, True)
                                            -- recurse into sub problem
                                            else case solver (newMoves gs') gs' newLog of 
                                                 -- recurse out in case of solution
                                                 (str, True) -> (str, True)
                                                 -- continue with next move
                                                 (str, False) -> solver ms gs str
                                           where
                                            -- move ball, get new gamestate
                                            gs' = flickBall ball dir gs
                                            newLog = addLog str ball dir
 -- add move log
addLog :: [String] -> BallIndex -> Direction -> [String]
addLog str (_,_,ch) dir = log : str
                        where 
                         log = "Fling! " ++ [ch] ++ " " ++ (map toLower $ show dir) 

                                           
-- get list of all moves of all balls in this gamestate
newMoves :: GameState -> [(BallIndex, Direction)]
newMoves gs@(balls,_,_) = concat [ [(ball, move) | 
                            move <- validMoves ball gs] |
                            ball <- balls ]

-- list of valid moves (directions) of current ball
validMoves :: BallIndex -> GameState -> [Direction]
validMoves ball gs@(bl,rm,cm) = map snd $ filter (\(_,dir) -> isValidMove ball dir gs) moveList
                             where 
                              (leftBall, rightBall) = getRowBalls ball rm
                              (downBall, topBall)   = getColBalls ball cm
                              moveList = filter (\(ball, _) -> isJust ball) 
                                         $ zip [leftBall, rightBall, topBall, downBall] [LEFT, RIGHT, TOP, DOWN]
                            
-- check if ball can be moved in a direction
-- an invalid move is when there is a ball next to the current ball in the given direction
-- or if the next ball is not a potential target
{-
isValidMove :: BallIndex -> Direction -> GameState -> Bool
isValidMove ball dir   gs = case maybeBallInDir of
                                Just ballInDir -> isPotentialTarget (newPos ball ballInDir dir) gs && noAdjBall
                                Nothing        -> noAdjBall
                          where
                            noAdjBall = not $ adjFn ball gs
                            maybeBallInDir = getBallInDir ball dir gs
                            adjFn = case dir of
                                        TOP   -> hasAdjTopBall
                                        DOWN  -> hasAdjDownBall
                                        LEFT  -> hasAdjLeftBall
                                        RIGHT -> hasAdjRightBall
-- is there any ball that can hit the target ?, optimization
isPotentialTarget :: BallIndex -> GameState -> Bool
--isPotentialTarget Nothing gs = False
isPotentialTarget ball (bl,rm,cm) = or $ map isJust [leftBall, rightBall, topBall, downBall]
            where 
                (leftBall, rightBall) = getRowBalls ball rm 
                (topBall,  downBall ) = getColBalls ball cm

-}

isValidMove ball TOP   = not . hasAdjTopBall  ball 
isValidMove ball DOWN  = not . hasAdjDownBall  ball 
isValidMove ball LEFT  = not . hasAdjLeftBall  ball 
isValidMove ball RIGHT = not . hasAdjRightBall ball 

{- BALL TRAVERSAL FUNCTIONS -}
-- flick a ball to one direction , if not adjacent to another ball in same direction
flickBall :: BallIndex -> Direction -> GameState -> GameState
flickBall ball dir gs = if (not $ (adjFn dir) ball gs)
                        -- if there is no ball adjacent to it in the same direction
                        then collideAndUpdate gs ball (getBallInDir ball dir gs) dir
                        -- otherwise, do nothing
                        else gs
                        where 
                         adjFn TOP   = hasAdjTopBall
                         adjFn DOWN  = hasAdjDownBall
                         adjFn LEFT  = hasAdjLeftBall
                         adjFn RIGHT = hasAdjRightBall

-- perform collision and change GameState accordingly
collideAndUpdate :: GameState -> BallIndex -> Maybe BallIndex -> Direction -> GameState
-- collide with edge, remove from gamestate
collideAndUpdate gs ball Nothing _ = removeBall ball gs
-- collide with another ball
collideAndUpdate gs@(bl,rm,cm) ball@(r,c,ch) (Just nball@(nr,nc,nch)) dir =
        -- get new position of ball, update gamestate and continue collision with next ball and so on
        collideAndUpdate (updateBall $ newPos ball nball dir) nball (nextBallInDir dir) dir
        where
         nextBallInDir dir = getBallInDir nball dir gs
         updateBall newPos = updateBallInfo ball newPos gs 

-- the new position of ball on collision with another ball
newPos :: BallIndex -> BallIndex -> Direction -> BallIndex
newPos (_, _, ch) (nr,nc,_) TOP   = (nr + 1, nc, ch)
newPos (_, _, ch) (nr,nc,_) DOWN  = (nr - 1, nc, ch)
newPos (_, _, ch) (nr,nc,_) LEFT  = (nr, nc + 1, ch)
newPos (_, _, ch) (nr,nc,_) RIGHT = (nr, nc - 1, ch)


{- INSERT AND UPDATE BALL FUNCTIONS -}
-- update gamestate with new ball information
updateBallInfo :: BallIndex -> BallIndex -> GameState -> GameState
updateBallInfo oldBall newBall gs = updatedState
                                    where
                                     -- remove old ball
                                     removedState = removeBall oldBall gs
                                     -- add new ball
                                     updatedState = insertBall newBall removedState

-- insert a new ball
insertBall :: BallIndex -> GameState -> GameState
insertBall ball@(r,c,ch) gs@(bl,rm,cm)= (ball:bl, newRowMap, newColMap)
                                      where
                                       -- insert into the row map
                                       newRowMap = M.insert r (ball : (fromJust $ M.lookup r rm)) rm
                                       -- insert into the col map
                                       newColMap = M.insert c (ball : (fromJust $ M.lookup c cm)) cm

{- REMOVE BALL FUNCTIONS -}
-- remove ball from game board
removeBall :: BallIndex -> GameState -> GameState
removeBall ball@(r,c,ch) (bl, rm, cm) = (removeBallIndex ball bl , removeFromMap ball r rm, removeFromMap ball c cm )

-- remove ball from the ball index
removeBallIndex :: BallIndex -> BallList -> BallList
removeBallIndex ball = filter (/= ball)

-- remove ball from a map
removeFromMap :: BallIndex -> Int -> RowMap -> RowMap 
removeFromMap ball i m = M.insert i (filter (/= ball) $ fromJust $ M.lookup i m) m 

{- ADJACENT BALL FUNCTIONS -}
-- is there another ball in the surrounding space ?
hasAdjacentBall :: BallIndex -> GameState -> Bool
hasAdjacentBall ball gs = 
        or [(fn ball gs) | fn <-[hasAdjLeftBall, hasAdjRightBall, hasAdjTopBall, hasAdjDownBall]] 
                               
-- check for adj ball in any specific direction
hasAdjTopBall   ball@(r,c,ch) gs = hasAdj ball gs (\(nr,_,_) -> nr == r - 1) TOP
hasAdjDownBall  ball@(r,c,ch) gs = hasAdj ball gs (\(nr,_,_) -> nr == r + 1) DOWN
hasAdjLeftBall  ball@(r,c,ch) gs = hasAdj ball gs (\(_,nc,_) -> nc == c - 1) LEFT
hasAdjRightBall ball@(r,c,ch) gs = hasAdj ball gs (\(_,nc,_) -> nc == c + 1) RIGHT

-- generic ajacent ball checking function
hasAdj :: BallIndex -> GameState -> (BallIndex -> Bool) -> Direction -> Bool
hasAdj ball gs f dir = case (getBallInDir ball dir gs) of 
                         Just (nr,nc,nch) -> f(nr,nc,nch)
                         Nothing -> False

{- BALL RETREIVAL FUNCTIONS -}
-- get the next ball in a particular direction near to another ball
getBallInDir :: BallIndex -> Direction -> GameState -> Maybe BallIndex
getBallInDir ball@(r,c,ch) dir (_,rm,cm) = case dir of 
                                              TOP   -> snd $ getColBalls ball cm
                                              DOWN  -> fst $ getColBalls ball cm
                                              LEFT  -> fst $ getRowBalls ball rm
                                              RIGHT -> snd $ getRowBalls ball rm

-- get closest balls to left and right of current ball
-- Steps: Get all balls in row -> Get a difference list between column of current ball and other balls
--        The closest balls will be the ones with least difference in either direction
--        To reverse direction, reverse sign
getRowBalls :: BallIndex -> RowMap -> (Maybe BallIndex, Maybe BallIndex)
getRowBalls (r,c,ch) m = (leftAdj, rightAdj)
                       where 
                        rowBalls = fromJust $ M.lookup r m  
                        diffList = map (\(nr, nc, nch) -> (nc - c, (nr,nc,nch))) rowBalls
                        rightAdj = getClosestBall diffList
                        leftAdj  = getClosestBall $ map (\(diff, x) -> (-diff,x)) diffList

-- get closest balls to top and down of current ball
-- Steps: Get all balls in col -> Get a difference list between row of current ball and other balls
--        The closest balls will be the ones with least difference in either direction
--        To reverse direction, reverse sign
getColBalls :: BallIndex -> ColMap -> (Maybe BallIndex, Maybe BallIndex)
getColBalls (r,c,ch) m = (topAdj, downAdj)
                       where 
                        colBalls = fromJust $ M.lookup c m  
                        diffList = map (\(nr, nc, nch) -> (nr - r, (nr,nc,nch))) colBalls
                        topAdj   = getClosestBall diffList
                        downAdj  = getClosestBall $ map (\(diff, x) -> (-diff,x)) diffList

-- get closest balls on either side of current ball
-- get +ve elements, sort and get first element
getClosestBall :: [(Int, BallIndex)] -> Maybe BallIndex
getClosestBall = safeHead . map snd . 
                 sortBy (\(diff1, _) (diff2, _) -> diff1 `compare` diff2) . 
                 filter (\(diff, _) -> diff > 0) 

{- UTIL FUNCTIONS -}
-- head wrapped in Maybe
safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:xs) = Just x


