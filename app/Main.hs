module Main where

import Lib
import System.Environment
import Control.Exception
import System.Directory (doesFileExist)
-- Custom Modules
--import GameDefs
--import GameParser
import GameSolver

-- main entry point
main :: IO ()
main = do
        -- get argument
        args <- getArgs
        -- solve problem
        sol <- solveProblem $ safeHead args
        -- print solution
        putStrLn sol
        return ()
        

