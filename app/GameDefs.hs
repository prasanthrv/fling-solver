module GameDefs where

import qualified Data.Map as M


-- A ball, (rowID, colID, character name)
-- ID starts from 1, rowID -> [1..7], colID -> [1..8]
type BallIndex = (Int, Int, Char)
-- A list of balls
type BallList = [BallIndex]
-- A map for fast lookup for balls in a row
type RowMap = M.Map Int BallList 
-- A map for fast lookup for balls in a column
type ColMap = M.Map Int BallList
-- State of the game at any moment
type GameState = (BallList, RowMap, ColMap)
-- Direction of motion of ball
data Direction = LEFT | RIGHT | DOWN | TOP
                deriving (Show, Eq, Ord, Read)



