-- Module for parsing game input data
module GameParser 
      (readGrid
      ,genMaps
      )
where

import qualified Data.Map as M
import Data.List (intercalate, sortBy)
import Data.Maybe (fromJust, isJust)
import GameDefs

-- maximum row and cols
rowCount = 8
colCount = 7

{- INPUT PARSING -}

-- convert given input into a list of balls
readGrid :: String -> [BallIndex]
readGrid  = filterEmptyPos . concat . (genRows 1) . genCols
        where
        -- remove empty ball postions and other whitespaces
        filterEmptyPos = filter (\(_,_, x) -> not $ (x == '.') || (x == ' ')) 
        -- create column entry for element
        genCols = map (zip [1..]) . lines 


-- create row entry for each element 
genRows :: Int -> [[(Int, Char)]] -> [[BallIndex]]
genRows _ [] = []
genRows r (x:xs) = map (\(c,ch) -> (r,c,ch)) x : genRows (r+1) xs  


{- GAME STATE GENERATION -}

-- Generate a empty map of lists
genEmptyIndexMap :: Int -> RowMap -> RowMap
genEmptyIndexMap 0 m = m
genEmptyIndexMap n m = genEmptyIndexMap (n-1) newMap
                    where
                        newMap = M.insert n [] m 

-- Generate row and column maps of grid
genMaps :: BallList -> (RowMap, ColMap)
genMaps indices = (genRowMap indices, genColMap indices)

-- Generate row map
genRowMap ::  BallList -> RowMap
genRowMap = genIndexMap rowCount (\acc (r,c,ch) -> M.insert r ((r,c,ch): (getList r acc)) acc)

-- Generate column map
genColMap ::  BallList -> ColMap
genColMap = genIndexMap colCount (\acc (r,c,ch) -> M.insert c ((r,c,ch): (getList c acc)) acc)

-- Get list at index from Map
getList i m = fromJust $ M.lookup i m

-- Generate index map for state
genIndexMap :: Int -> (RowMap -> BallIndex -> RowMap) -> BallList -> ColMap
genIndexMap maxIndex f = foldl f (genEmptyIndexMap maxIndex M.empty)

-- Convert map to print form, debug purposes
printIndexMap :: Int -> RowMap -> String
printIndexMap maxIndex rm = header ++ printIndex maxIndex ++ footer
            where
                header = "{\n"
                footer = "}\n"
                printIndex 0 = ""
                printIndex n = show n ++ ": " ++ ((intercalate " ") $ map show $ fromJust $ M.lookup n rm) ++ "\n" ++ printIndex (n -1)




