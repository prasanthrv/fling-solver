import System.Directory (getDirectoryContents)
import Data.List (isInfixOf, isPrefixOf)
import System.IO
import GameSolver
import Test.HUnit

-- edit to change directory
rootTestDir = "/Users/dare/projects/haskell/fling"
solvTestDir = rootTestDir ++ "/test/test_data/solv/"
nsolvTestDir = rootTestDir ++ "/test/test_data/nsolv/"

-- main test fn
main :: IO ()
main = do
        putStrLn " "
        putStrLn "Running tests on solvable data"
        runTests solvTestDir assertSolvableProblem
        putStrLn "Running tests on non solvable data"
        runTests nsolvTestDir assertUnsolvableProblem


-- run tests on a particular directory with a helper test function
runTests path testFn = do
                        fileNames <- getDirectoryContents path 
                        -- remove ., .., etc
                        let validNames = filter (\x -> not $ isPrefixOf "." x) fileNames
                        let files = map (path ++) validNames 
                        putStrLn $ " Running tests on " ++ (show $ length files) ++ " test cases" 
                        mapM testFn files
                        return()

-- helper functions
-- does not have a "no solution " text
assertSolvableProblem   = assertProblem (\sol -> not $ isInfixOf "no solution" sol) 
-- has a "no solution " text
assertUnsolvableProblem = assertProblem (\sol -> isInfixOf "no solution" sol) 

-- run test on a particular test file
assertProblem :: (String->Bool) -> String -> Assertion
assertProblem solFn filePath  
            | head filePath == '.' = return ()
            | otherwise = do
                          --putStrLn $ "Running test on " ++ filePath
                          sol <- solveProblem (Just filePath)
                          --putStrLn sol
                          assertBool "Could not solve test case" (solFn sol)





